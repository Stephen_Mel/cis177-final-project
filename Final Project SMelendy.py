'''
Author: Stephen Melendy

Date: August 1st, 2016

Program: Fizbin


Note to those reading this code:

I am sorry. It is a nightmare to read. Unfortunately the structure is very linear, some functions are defined early, but
most of the program just reads from start to finish... there is no Main function. To navigate through the program, just 'x' out
the window.

Also please install the "Chiller" font before playing.
'''

import random
from tkinter import *



class StandardDeck:

    # The following are static variables of the StandardDeck class
    suits = ["Spades", "Hearts", "Diamonds", "Clubs"]
    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King"]
    size = len(suits) * len(ranks)

class Card:
    # Initializer (constructor)
    def __init__(self, value):

        self.__value = value
        self.__suit = StandardDeck.suits[self.__value // 13]
        self.__rank = StandardDeck.ranks[self.__value % 13]

    # Return the string representation of this card
    def __str__(self):

        string = self.__rank + " of " + self.__suit
        return string

    # Return the integer value of this card (Accessor method for __value)
    def get_card_int(self):
        return self.__value

    # Return the rank of the card ("2", "Ace", "King", etc)
    def get_card_rank(self):
        return self.__rank

    #Return the suit of the card
    def get_card_suit(self):
        return self.__suit

#This is a Player class. Currently the game constructs 2 player instances.
class Player:
    def __init__(self, handSize=7):
        self.handSize = handSize
        self.hand = []
        self.score = 0        

    # This fills the hand list created in the init. Requires a 'deck' variable to be created
    # drawHand is written so that it will draw cards up to the hand limit. Each card drawn will be removed from the deck.
    def drawHand(self):
        count = 0
        while len(self.hand) < self.handSize:
            self.hand.append(deck[0])
            deck.remove(deck[0])
            count += 1

    # Adds to the player's score... not completely necessary since I don't have the score as __score.
    def addScore(self, s=2):
        self.score += s
    
    def getScore(self):
        return self.score


# Create a deck and draw cards to fill the deck.
deck = []
for i in range(0, StandardDeck.size):
    deck.append(i)

random.shuffle(deck)

# Create two player instances and draw their hand
Player1 = Player()
Player1.drawHand()       

Player2 = Player()
Player2.drawHand()


# This is the first Tkinter window created. It is just a silly start screen.
# All that is put into the window are a few Text labels
# 'x' out of the window to proceed.
def startScreen():
    window = Tk()
    window.title("Start Screen")

    frame = Frame(window)
    frame.pack()
    Label(frame, text="While looking for the bathroom in an old-old bar, you and a friend enter a strange stone room. "
                      "\n The door slams shut behind you as the lights turn off. "
                      "\n From the darkness a glowing face emerges. "
                      "\n Its voice sends shivers down your spine.", font="Helvetica 12 bold").pack()

    frame2 = Frame(window)
    frame2.pack()
    Label(frame2, text='"Welcome, time to play a game. \n The player who gives me the cards I ask for shall live. '
                       '\n Choose wisely!"', font="Chiller 20 bold", fg='red').pack(padx=50, pady=50)
    window.mainloop()


# This the second screen, it just defines some general rules for the game. 'x' out of the window to proceed.
def rulesScreen():
    window = Tk()
    window.title("Start Screen")

    frame = Frame(window)
    frame.pack()
    Label(frame, text="You are trapped in Fizbin's dungeon. \n"
                      "Each round Fizbin will request certain groups of cards. \n"
                      "Depending on his mood, he will be more or less likely to be pleased with the cards you give. \n"
                      "Accepted cards will grant points, rejected cards will deduct points. \n"
                      "Good luck!", font="Helvetica 14 bold").pack(padx=30, pady=100)

    window.mainloop()

# This is the first Tkinter window that is a round of the actual game.
def submitSuit():

    #This generates the suit that will be requested for the round of Fizbin.
    desiredSuit = StandardDeck.suits[random.randint(0,3)]  
    
    
    # Creates the window and names it with the desired suit.
    window = Tk()
    window.title("Submit " + desiredSuit)

    # Fizbin's mood is determined by his facial expression. This creates photoimages of his different expressions.
    happyFace = PhotoImage(file = "Happy Face.gif")
    neutralFace = PhotoImage(file = "Neutral Face.gif")
    angryFace = PhotoImage(file = "Angry Face.gif")

    # Create a canvas that will hold the image of Fizbin's facial expression.
    canvas = Canvas(window, width = 200, height=200, bg="white")
    canvas.pack()

    # Establishes Fizbin's mood and places a different image into the Canvas depending on his modd.
    mood = random.randint(1,8)        
    if mood >= 1 and mood < 3:  
        canvas.create_image(100, 100, image=angryFace)
    elif mood >= 3 and mood < 6:
        canvas.create_image(100, 100, image=neutralFace)
    elif mood >= 6: 
        canvas.create_image(100, 100, image=happyFace)
     
    # Creates a frame that shows Fizbin's request to the user.
    frame1 = Frame(window)
    frame1.pack()     
    fizbinSpeaks = Label(frame1, text="Submit Any " + desiredSuit + "!", font="Chiller 28 bold")     
    fizbinSpeaks.pack()

    # Creates a frame that displays "Player 1" above their cards
    frame4 = Frame(window)
    frame4.pack()      
    player1Name = Label(frame4, text="Player 1", font="Helvetica 16 bold")    
    player1Name.pack()

    # Creates a series of tkinter PhotoImages that represent the cards in Player 1's hand
    cardImage1 = PhotoImage(file="cards/" + str(Player1.hand[0]+1) + ".gif")
    cardImage2 = PhotoImage(file="cards/" + str(Player1.hand[1]+1) + ".gif")
    cardImage3 = PhotoImage(file="cards/" + str(Player1.hand[2]+1) + ".gif")
    cardImage4 = PhotoImage(file="cards/" + str(Player1.hand[3]+1) + ".gif")
    cardImage5 = PhotoImage(file="cards/" + str(Player1.hand[4]+1) + ".gif")
    cardImage6 = PhotoImage(file="cards/" + str(Player1.hand[5]+1) + ".gif") 
    cardImage7 = PhotoImage(file="cards/" + str(Player1.hand[6]+1) + ".gif") 
    
    # Creates IntVar() objects that will be used for the checkboxes in the window. IntVar() essential returns a 1 if the checkbox
    #    is checked, and a 0 if the check box is not checked.
    v1 = IntVar()
    v2 = IntVar()
    v3 = IntVar()
    v4 = IntVar()
    v5 = IntVar()
    v6 = IntVar()
    v7 = IntVar()
                         
    varList = [v1, v2, v3, v4, v5, v6, v7]       

    #Creates a frame for the check boxes that will be used for Player 1's cards
    frame2 = Frame(window)
    frame2.pack()

    # Creates a check box with an associated card image and packs it into the frame
    c1 = Checkbutton(frame2, variable=v1, image=cardImage1).pack(side=LEFT)
    c2 = Checkbutton(frame2, variable=v2, image=cardImage2).pack(side=LEFT)
    c3 = Checkbutton(frame2, variable=v3, image=cardImage3).pack(side=LEFT)
    c4 = Checkbutton(frame2, variable=v4, image=cardImage4).pack(side=LEFT)
    c5 = Checkbutton(frame2, variable=v5, image=cardImage5).pack(side=LEFT)
    c6 = Checkbutton(frame2, variable=v6, image=cardImage6).pack(side=LEFT)      
    c7 = Checkbutton(frame2, variable=v7, image=cardImage7).pack(side=LEFT)       
    
############## All this code is almost identical to the code above but for Player 2's cards ##########
    frame5 = Frame(window)
    frame5.pack()      
    player2Name = Label(frame5, text="Player 2", font="Helvetica 16 bold")    
    player2Name.pack()
    
    
    cardImage8 = PhotoImage(file="cards/" + str(Player2.hand[0]+1) + ".gif")
    cardImage9 = PhotoImage(file="cards/" + str(Player2.hand[1]+1) + ".gif")
    cardImage10 = PhotoImage(file="cards/" + str(Player2.hand[2]+1) + ".gif")
    cardImage11 = PhotoImage(file="cards/" + str(Player2.hand[3]+1) + ".gif")
    cardImage12 = PhotoImage(file="cards/" + str(Player2.hand[4]+1) + ".gif")
    cardImage13 = PhotoImage(file="cards/" + str(Player2.hand[5]+1) + ".gif") 
    cardImage14 = PhotoImage(file="cards/" + str(Player2.hand[6]+1) + ".gif") 
           
    v8 = IntVar()
    v9 = IntVar()
    v10 = IntVar()
    v11 = IntVar()
    v12 = IntVar()
    v13 = IntVar()
    v14 = IntVar()
                         
    varList2 = [v8, v9, v10, v11, v12, v13, v14]       
                
    frame3 = Frame(window)
    frame3.pack()
            
    c8 = Checkbutton(frame3, variable=v8, image=cardImage8).pack(side=LEFT)
    c9 = Checkbutton(frame3, variable=v9, image=cardImage9).pack(side=LEFT)
    c10 = Checkbutton(frame3, variable=v10, image=cardImage10).pack(side=LEFT)
    c11 = Checkbutton(frame3, variable=v11, image=cardImage11).pack(side=LEFT)
    c12 = Checkbutton(frame3, variable=v12, image=cardImage12).pack(side=LEFT)
    c13 = Checkbutton(frame3, variable=v13, image=cardImage13).pack(side=LEFT)      
    c14 = Checkbutton(frame3, variable=v14, image=cardImage14).pack(side=LEFT)       
##############################


    # This just makes a frame for some text to instuct the player.
    frame6 = Frame(window)
    frame6.pack()      
    Exit = Label(frame6, text="Check boxes for all cards you wish to submit "
                              "\n Close window to submit cards and proceed to the next round", font="Helvetica 10 bold")
    Exit.pack()
    window.mainloop()

    # Creates empty lists that will gather cards submitted by the checkboxes and then removes them from the Player's hand
    submitted = []
    submitted2 = []
    removeList = []
    removeList2 = []

    # This goes through the varList of all the IntVar objects and adds the cards that have their corresponding checkbox
            #  checked and adds them to the 'submitted' list. It also adds the cards to be removed to a 'removeList'
    for i in range(0, len(varList)):
        if varList[i].get() == 1:
            submitted.append(Player1.hand[i])
            removeList.append(Player1.hand[i])

    # This removes all the cards from the player's hand that was is in the removeList.
    # I did this two step process because my indexing got all wonky when I tried to append and remove in one loop.
    for i in range(0, len(removeList)):
        Player1.hand.remove(removeList[i])

    # Generates an integer that will be compared against Fizbin's mood to determine whether he accepts the submitted cards.
    luck = random.randint(1,8)
    
    # This creates a loop that first checks if the luck is less than the mood (luck <= mood means that the player CAN score points)
    # If the luck condition is met, then the actual scoring begins. Essentially the card suit of any cards submitted is compared
    # to the card suit of the 'desiredSuit' that has established at the beginning of the window.
    for i in range(len(submitted)):
        if luck <= mood:    
        
            if Card(submitted[i]).get_card_suit() != desiredSuit:
                print(str(Card(submitted[i])) + " is not the right suit")
                Player1.score -= 1
        
            if Card(submitted[i]).get_card_suit() == desiredSuit:
                print(str(Card(submitted[i])) + " is the right suit")
                Player1.addScore()
        elif luck > mood:
            print("Your cards displease me!")
            Player1.score -= 1
    
############## This is the same as before but is for player 2 ####################
    for i in range(0, len(varList2)):
        if varList2[i].get() == 1:
            submitted2.append(Player2.hand[i])
            removeList.append(Player2.hand[i])

    for i in range(0, len(removeList2)):
        Player2.hand.remove(removeList2[i])

    luck = random.randint(1, 8)

    for i in range(len(submitted2)):
        if luck <= mood:    
        
            if Card(submitted2[i]).get_card_suit() != desiredSuit:
                print(str(Card(submitted2[i])) + " is not the right suit")
                Player2.score -= 1
        
            if Card(submitted2[i]).get_card_suit() == desiredSuit:
                print(str(Card(submitted2[i])) + " is the right suit")
                Player2.addScore()
        elif luck > mood:
            print("Your cards displease me!") 
            Player2.score -= 1               
 #############################################################################

    # The text displays in the console to see the result and each player draws a new hand
    print("Player1's score is: " + str(Player1.getScore()))
    print("Player2's score is: " + str(Player2.getScore()))
    Player1.drawHand()
    Player2.drawHand()


def submitPairs():

#############   All of this window setup is the same as before except the text asks for 'pairs' instead of a suit.###########
    window = Tk()
    window.title("Submit Pairs")
    
    happyFace = PhotoImage(file = "Happy Face.gif")
    neutralFace = PhotoImage(file = "Neutral Face.gif")
    angryFace = PhotoImage(file = "Angry Face.gif")
    
    canvas = Canvas(window, width = 200, height=200, bg="white")
    canvas.pack()
    
    frame1 = Frame(window)
    frame1.pack()     
    fizbinSpeaks = Label(frame1, text="Submit Any Pairs!", font="Chiller 28 bold")     
    fizbinSpeaks.pack()        
        
    
    mood = random.randint(1,8)        
    if mood >= 1 and mood < 3:  
        canvas.create_image(100, 100, image=angryFace)
    elif mood >= 3 and mood < 6:
        canvas.create_image(100, 100, image=neutralFace)
    elif mood >= 6: 
        canvas.create_image(100, 100, image=happyFace)
     

    frame4 = Frame(window)
    frame4.pack()      
    player1Name = Label(frame4, text="Player 1", font="Helvetica 16 bold")    
    player1Name.pack()
        
    cardImage1 = PhotoImage(file="cards/" + str(Player1.hand[0]+1) + ".gif")
    cardImage2 = PhotoImage(file="cards/" + str(Player1.hand[1]+1) + ".gif")
    cardImage3 = PhotoImage(file="cards/" + str(Player1.hand[2]+1) + ".gif")
    cardImage4 = PhotoImage(file="cards/" + str(Player1.hand[3]+1) + ".gif")
    cardImage5 = PhotoImage(file="cards/" + str(Player1.hand[4]+1) + ".gif")
    cardImage6 = PhotoImage(file="cards/" + str(Player1.hand[5]+1) + ".gif") 
    cardImage7 = PhotoImage(file="cards/" + str(Player1.hand[6]+1) + ".gif") 
    
           
    v1 = IntVar()
    v2 = IntVar()
    v3 = IntVar()
    v4 = IntVar()
    v5 = IntVar()
    v6 = IntVar()
    v7 = IntVar()
                         
    varList = [v1, v2, v3, v4, v5, v6, v7]       
                
    frame2 = Frame(window)
    frame2.pack()
            
    c1 = Checkbutton(frame2, variable=v1, image=cardImage1).pack(side=LEFT)
    c2 = Checkbutton(frame2, variable=v2, image=cardImage2).pack(side=LEFT)
    c3 = Checkbutton(frame2, variable=v3, image=cardImage3).pack(side=LEFT)
    c4 = Checkbutton(frame2, variable=v4, image=cardImage4).pack(side=LEFT)
    c5 = Checkbutton(frame2, variable=v5, image=cardImage5).pack(side=LEFT)
    c6 = Checkbutton(frame2, variable=v6, image=cardImage6).pack(side=LEFT)      
    c7 = Checkbutton(frame2, variable=v7, image=cardImage7).pack(side=LEFT)       
    
    frame5 = Frame(window)
    frame5.pack()      
    player2Name = Label(frame5, text="Player 2", font="Helvetica 16 bold")    
    player2Name.pack()
    
    
    cardImage8 = PhotoImage(file="cards/" + str(Player2.hand[0]+1) + ".gif")
    cardImage9 = PhotoImage(file="cards/" + str(Player2.hand[1]+1) + ".gif")
    cardImage10 = PhotoImage(file="cards/" + str(Player2.hand[2]+1) + ".gif")
    cardImage11 = PhotoImage(file="cards/" + str(Player2.hand[3]+1) + ".gif")
    cardImage12 = PhotoImage(file="cards/" + str(Player2.hand[4]+1) + ".gif")
    cardImage13 = PhotoImage(file="cards/" + str(Player2.hand[5]+1) + ".gif") 
    cardImage14 = PhotoImage(file="cards/" + str(Player2.hand[6]+1) + ".gif") 
    
           
    v8 = IntVar()
    v9 = IntVar()
    v10 = IntVar()
    v11 = IntVar()
    v12 = IntVar()
    v13 = IntVar()
    v14 = IntVar()
                         
    varList2 = [v8, v9, v10, v11, v12, v13, v14]       
                
    frame3 = Frame(window)
    frame3.pack()
            
    c8 = Checkbutton(frame3, variable=v8, image=cardImage8).pack(side=LEFT)
    c9 = Checkbutton(frame3, variable=v9, image=cardImage9).pack(side=LEFT)
    c10 = Checkbutton(frame3, variable=v10, image=cardImage10).pack(side=LEFT)
    c11 = Checkbutton(frame3, variable=v11, image=cardImage11).pack(side=LEFT)
    c12 = Checkbutton(frame3, variable=v12, image=cardImage12).pack(side=LEFT)
    c13 = Checkbutton(frame3, variable=v13, image=cardImage13).pack(side=LEFT)      
    c14 = Checkbutton(frame3, variable=v14, image=cardImage14).pack(side=LEFT)
    
    frame6 = Frame(window)
    frame6.pack()      
    Exit = Label(frame6, text="Check boxes for all cards you wish to submit \n Close window to submit cards and proceed to the next round", font="Helvetica 10 bold") 
    Exit.pack()
    
    window.mainloop()


    luck = random.randint(1,8)       
    submitted = []
    submitted2 = []

    removeList = []
    removeList2 = []
    
    for i in range(0, len(varList)):
        if varList[i].get() == 1:
            submitted.append(Player1.hand[i])
            removeList.append(Player1.hand[i])

    for i in range(0, len(removeList)):
        Player1.hand.remove(removeList[i])
#####################################################################################


    # This is where the code deviates from the previous round.
    # An empty list called 'submittedRanks' is created.
    submittedRanks = []

    # Gets the card rank for each submitted card and puts it into the submittedRanks list
    for i in range(len(submitted)):
        r = Card(submitted[i]).get_card_rank()    
        submittedRanks.append(r) 

    # Again compares Fizbin's mood to your luck. If that condition is met, scoring begins.
    # For each card in submitted ranks, its count is stored in variable 'c'
    # If 'c' is greater than 1, it means that the same rank was submitted more than once (this mean you submitted a pair!)
    # 3 and 4 of-a-kind also count
    for i in range(len(submitted)):
        if luck <= mood:
            c = submittedRanks.count(submittedRanks[i])
            if c > 1:
                Player1.score += 2*c
                print("You have a pair!")
            if c == 1:
                print("No pairs submitted")
                Player1.score -= 1
        elif luck > mood:
            print("Your cards displease me!")         
            Player1.score -= 1
    
    

###### Same as scoring for Player 1, but for Player 2#######################
    for i in range(0, len(varList2)):
        if varList2[i].get() == 1:
            submitted2.append(Player2.hand[i])
            removeList.append(Player2.hand[i])

    for i in range(0, len(removeList2)):
        Player2.hand.remove(removeList2[i])
    
    submittedRanks2 = []
    
    for i in range(len(submitted2)):
        r = Card(submitted2[i]).get_card_rank()    
        submittedRanks2.append(r)

    luck = random.randint(1, 8)

    for i in range(len(submitted2)):
        if luck <= mood:
            c = submittedRanks2.count(submittedRanks2[i])
            if c > 1:
                Player2.score += 2*c
                print("you have a pair!")
            if c == 1:
                print("No pairs submitted")
                Player2.score -= 1
        elif luck > mood:
            print("Your cards displease me!")
            Player2.score -= 1
################################################################################

        
    print("Player1's score is: " + str(Player1.getScore()))
    print("Player2's score is: " + str(Player2.getScore()))
    Player1.drawHand()
    Player2.drawHand()


def terrifyingGroup():

    ###########   This is another round window setup, identical to the others but with minor text changes #############
    window = Tk()
    window.title("Submit a terrifying group of cards")

    happyFace = PhotoImage(file="Happy Face.gif")
    neutralFace = PhotoImage(file="Neutral Face.gif")
    angryFace = PhotoImage(file="Angry Face.gif")

    canvas = Canvas(window, width=200, height=200, bg="white")
    canvas.pack()

    frame1 = Frame(window)
    frame1.pack()
    fizbinSpeaks = Label(frame1, text="Submit a Terrifying Group of Cards!", font="Chiller 28 bold")
    fizbinSpeaks.pack()

    mood = random.randint(1, 8)
    if mood >= 1 and mood < 3:
        canvas.create_image(100, 100, image=angryFace)
    elif mood >= 3 and mood < 6:
        canvas.create_image(100, 100, image=neutralFace)
    elif mood >= 6:
        canvas.create_image(100, 100, image=happyFace)

    frame4 = Frame(window)
    frame4.pack()
    player1Name = Label(frame4, text="Player 1", font="Helvetica 16 bold")
    player1Name.pack()

    cardImage1 = PhotoImage(file="cards/" + str(Player1.hand[0] + 1) + ".gif")
    cardImage2 = PhotoImage(file="cards/" + str(Player1.hand[1] + 1) + ".gif")
    cardImage3 = PhotoImage(file="cards/" + str(Player1.hand[2] + 1) + ".gif")
    cardImage4 = PhotoImage(file="cards/" + str(Player1.hand[3] + 1) + ".gif")
    cardImage5 = PhotoImage(file="cards/" + str(Player1.hand[4] + 1) + ".gif")
    cardImage6 = PhotoImage(file="cards/" + str(Player1.hand[5] + 1) + ".gif")
    cardImage7 = PhotoImage(file="cards/" + str(Player1.hand[6] + 1) + ".gif")

    v1 = IntVar()
    v2 = IntVar()
    v3 = IntVar()
    v4 = IntVar()
    v5 = IntVar()
    v6 = IntVar()
    v7 = IntVar()

    varList = [v1, v2, v3, v4, v5, v6, v7]

    frame2 = Frame(window)
    frame2.pack()

    c1 = Checkbutton(frame2, variable=v1, image=cardImage1).pack(side=LEFT)
    c2 = Checkbutton(frame2, variable=v2, image=cardImage2).pack(side=LEFT)
    c3 = Checkbutton(frame2, variable=v3, image=cardImage3).pack(side=LEFT)
    c4 = Checkbutton(frame2, variable=v4, image=cardImage4).pack(side=LEFT)
    c5 = Checkbutton(frame2, variable=v5, image=cardImage5).pack(side=LEFT)
    c6 = Checkbutton(frame2, variable=v6, image=cardImage6).pack(side=LEFT)
    c7 = Checkbutton(frame2, variable=v7, image=cardImage7).pack(side=LEFT)

    frame5 = Frame(window)
    frame5.pack()
    player2Name = Label(frame5, text="Player 2", font="Helvetica 16 bold")
    player2Name.pack()

    cardImage8 = PhotoImage(file="cards/" + str(Player2.hand[0] + 1) + ".gif")
    cardImage9 = PhotoImage(file="cards/" + str(Player2.hand[1] + 1) + ".gif")
    cardImage10 = PhotoImage(file="cards/" + str(Player2.hand[2] + 1) + ".gif")
    cardImage11 = PhotoImage(file="cards/" + str(Player2.hand[3] + 1) + ".gif")
    cardImage12 = PhotoImage(file="cards/" + str(Player2.hand[4] + 1) + ".gif")
    cardImage13 = PhotoImage(file="cards/" + str(Player2.hand[5] + 1) + ".gif")
    cardImage14 = PhotoImage(file="cards/" + str(Player2.hand[6] + 1) + ".gif")

    v8 = IntVar()
    v9 = IntVar()
    v10 = IntVar()
    v11 = IntVar()
    v12 = IntVar()
    v13 = IntVar()
    v14 = IntVar()

    varList2 = [v8, v9, v10, v11, v12, v13, v14]

    frame3 = Frame(window)
    frame3.pack()

    c8 = Checkbutton(frame3, variable=v8, image=cardImage8).pack(side=LEFT)
    c9 = Checkbutton(frame3, variable=v9, image=cardImage9).pack(side=LEFT)
    c10 = Checkbutton(frame3, variable=v10, image=cardImage10).pack(side=LEFT)
    c11 = Checkbutton(frame3, variable=v11, image=cardImage11).pack(side=LEFT)
    c12 = Checkbutton(frame3, variable=v12, image=cardImage12).pack(side=LEFT)
    c13 = Checkbutton(frame3, variable=v13, image=cardImage13).pack(side=LEFT)
    c14 = Checkbutton(frame3, variable=v14, image=cardImage14).pack(side=LEFT)

    frame6 = Frame(window)
    frame6.pack()
    Exit = Label(frame6,
                 text="Check boxes for all cards you wish to submit \n Close window to submit cards and proceed to the next round",
                 font="Helvetica 10 bold")
    Exit.pack()

    window.mainloop()
############################################################################################

    # Determines luck to be compared against Fizbin's mood.
    luck = random.randint(1, 8)
    submitted = []
    submitted2 = []

    removeList = []
    removeList2 = []


# Add's cards to the 'submitted' list that had their check boxes checked. Also removes them from the player's hand
    for i in range(0, len(varList)):
        if varList[i].get() == 1:
            submitted.append(Player1.hand[i])
            removeList.append(Player1.hand[i])

    for i in range(0, len(removeList)):
        Player1.hand.remove(removeList[i])


# Creates a list of the submitted card's ranks
    submittedRanks = []

    for i in range(len(submitted)):
        r = Card(submitted[i]).get_card_rank()
        submittedRanks.append(r)

# First decides if enough cards were submitted
# Then check luck vs mood
# Then determines whether the submitted cards are a "Terrifying Group"
# Yes this is silly, but a Terrifying Group are either cards whose ranks are 6-6-6 or King-Queen
    if len(submitted) > 1:

        if luck <= mood:
            if submittedRanks[0] == "Queen" and submittedRanks[1] == "King":
                print("A King and Queen certainly are a terrifying pair")
                Player1.addScore(8)
            elif submittedRanks[0] == "King" and submittedRanks[1] == "Queen":
                print("A King and Queen certainly are a terrifying pair")
                Player1.addScore(8)
            elif submittedRanks[0] == "6" and submittedRanks[1] == "6" and submittedRanks[2] == "6":
                print("Superstitious eh? 666 is a terrifying group of cards")
                Player1.addScore(16)

            else:
                print("That is not a terrifying group")
                Player1.score -= len(submitted)

        elif luck > mood:
            print("Your cards displease me!")
            Player1.score -= len(submitted)

###### Scoring for Player 2 #############################
    for i in range(0, len(varList2)):
        if varList2[i].get() == 1:
            submitted2.append(Player2.hand[i])
            removeList.append(Player2.hand[i])

    for i in range(0, len(removeList2)):
        Player2.hand.remove(removeList2[i])

    submittedRanks2 = []

    for i in range(len(submitted2)):
        r = Card(submitted2[i]).get_card_rank()
        submittedRanks2.append(r)

    luck = random.randint(1, 8)

    if len(submitted2) > 1:

        if luck <= mood:
            if submittedRanks2[0] == "Queen" and submittedRanks2[1] == "King":
                print("Player 2: A King and Queen certainly are a terrifying pair")
                Player2.addScore(8)
            elif submittedRanks2[0] == "King" and submittedRanks2[1] == "Queen":
                print("Player 2: A King and Queen certainly are a terrifying pair")
                Player2.addScore(8)
            elif submittedRanks2[0] == "6" and submittedRanks2[1] == "6" and submittedRanks2[2] == "6":
                print("Player 2: Superstitious eh? 666 is a terrifying group of cards")
                Player2.addScore(16)
            else:
                print("Player 2: That is not a terrifying group")
                Player2.score -= len(submitted2)

        elif luck > mood:
            print("Player 2: Your cards displease me!")
            Player2.score -= len(submitted2)

############################################################################

    print("Player1's score is: " + str(Player1.getScore()))
    print("Player2's score is: " + str(Player2.getScore()))
    Player1.drawHand()
    Player2.drawHand()

# This creates a final score window that simply displays the player's scores and determines a winner.
def finalScore():
    window = Tk()
    window.title("Score Screen")

    frame = Frame(window)
    frame.pack()
    Label(frame, text=("Player 1's score is: " + str(Player1.getScore())), font="Chiller 24 bold").pack()

    frame2 = Frame(window)
    frame2.pack()
    Label(frame2, text=("Player 2's score is: " + str(Player2.getScore())), font="Chiller 24 bold").pack()

    frame3 = Frame(window)
    frame3.pack()

    if Player1.getScore() > Player2.getScore():
        Label(frame2, text="Player 2 Loses!  (and dies)", font="Chiller 32 bold", fg='red').pack(padx=100, pady=80)

    elif Player2.getScore() > Player1.getScore():
        Label(frame2, text="Player 1 Loses!  (and dies)", font="Chiller 32 bold", fg='red').pack(padx=100, pady=80)

    elif Player2.getScore() == Player1.getScore():
        Label(frame2, text="It's a tie!   Both players lose!", font="Chiller 32 bold", fg='red').pack(padx=100, pady=80)

    window.mainloop()


# Here the different screens are called. I probably should have just put these in a Main function
startScreen()

rulesScreen()

gameRounds = [submitPairs, submitPairs, submitSuit, submitSuit, submitSuit, terrifyingGroup]

# Establishes the number of rounds
numberOfRounds = 5

# This randomizes the rounds so that the players won't know which round is displayed.
count = 0
while count < numberOfRounds:
    gameRounds[random.randint(0, len(gameRounds)-1)]()
    count += 1

finalScore()
