import random
from tkinter import *



class StandardDeck:

    # The following are static variables of the StandardDeck class
    suits = ["Spades", "Hearts", "Diamonds", "Clubs"]
    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King"]
    size = len(suits) * len(ranks)

class Card:
    # Initializer (constructor)
    def __init__(self, value):

        self.__value = value
        self.__suit = StandardDeck.suits[self.__value // 13]
        self.__rank = StandardDeck.ranks[self.__value % 13]

    # Return the string representation of this card
    def __str__(self):

        string = self.__rank + " of " + self.__suit

        return string
    # Return the integer value of this card (Accessor method for __value)
    def get_card_int(self):
        return self.__value

    def get_card_suit(self):
        return self.__suit

class Player:
    def __init__(self, handSize=7):
        self.handSize = handSize
        self.hand = []

    def drawHand(self):
        count = 0
        while len(self.hand) < self.handSize:
            self.hand.append(deck[0])
            deck.remove(deck[0])
            count += 1


deck = []


for i in range(0, StandardDeck.size):
    card = Card(i)      # constructs a Card object ... calls the initializer
    #print(card, "(" + str(Card.get_card_int(card)) + ")")
    deck.append(card)


random.shuffle(deck)

Stephen = Player()

Stephen.drawHand()


window = Tk()
window.title("Your Hand")
Label(window, text="Please submit any hearts:").pack()

#frame = Frame(window)
#frame.pack()
'''
cardValue1 = Stephen.hand[0].get_card_int()
cardImage1 = PhotoImage(file="cards/" + str(cardValue1+1) + ".gif")
Label(frame, image=cardImage1).pack(side=LEFT)

cardValue2 = Stephen.hand[1].get_card_int()
cardImage2 = PhotoImage(file="cards/" + str(cardValue2+1) + ".gif")
Label(frame, image=cardImage2).pack(side=LEFT)

cardValue3 = Stephen.hand[2].get_card_int()
cardImage3 = PhotoImage(file="cards/" + str(cardValue3+1) + ".gif")
Label(frame, image=cardImage3).pack(side=LEFT)

cardValue4 = Stephen.hand[3].get_card_int()
cardImage4 = PhotoImage(file="cards/" + str(cardValue4+1) + ".gif")
Label(frame, image=cardImage4).pack(side=LEFT)

cardValue5 = Stephen.hand[4].get_card_int()
cardImage5 = PhotoImage(file="cards/" + str(cardValue5+1) + ".gif")
Label(frame, image=cardImage5).pack(side=LEFT)

cardValue6 = Stephen.hand[5].get_card_int()
cardImage6 = PhotoImage(file="cards/" + str(cardValue6+1) + ".gif")
Label(frame, image=cardImage6).pack(side=LEFT)

cardValue7 = Stephen.hand[6].get_card_int()
cardImage7 = PhotoImage(file="cards/" + str(cardValue7+1) + ".gif")
Label(frame, image=cardImage2).pack(side=LEFT)


frame2 = Frame(window)
frame2.pack()

Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
Checkbutton(frame2).pack(side=LEFT)
'''

cardValue1 = Stephen.hand[0].get_card_int()
cardImage1 = PhotoImage(file="cards/" + str(cardValue1+1) + ".gif")


cardValue2 = Stephen.hand[1].get_card_int()
cardImage2 = PhotoImage(file="cards/" + str(cardValue2+1) + ".gif")


cardValue3 = Stephen.hand[2].get_card_int()
cardImage3 = PhotoImage(file="cards/" + str(cardValue3+1) + ".gif")
'''
cardValue4 = Stephen.hand[3].get_card_int()
cardImage4 = PhotoImage(file="cards/" + str(cardValue4+1) + ".gif")

cardValue5 = Stephen.hand[4].get_card_int()
cardImage5 = PhotoImage(file="cards/" + str(cardValue5+1) + ".gif")

cardValue6 = Stephen.hand[5].get_card_int()
cardImage6 = PhotoImage(file="cards/" + str(cardValue6+1) + ".gif")

cardValue7 = Stephen.hand[6].get_card_int()
cardImage7 = PhotoImage(file="cards/" + str(cardValue7+1) + ".gif")
'''

frame2 = Frame(window)
frame2.pack()

v1 = IntVar()
v2 = IntVar()
v3 = IntVar()

varList = [v1, v2, v3]


c1 = Checkbutton(frame2, variable=v1, image=cardImage1).pack(side=LEFT)
c2 = Checkbutton(frame2, variable=v2, image=cardImage2).pack(side=LEFT)
c3 = Checkbutton(frame2, variable=v3, image=cardImage3).pack(side=LEFT)
#c4 = Checkbutton(frame2, image = cardImage4).pack(side=LEFT)
#c5 = Checkbutton(frame2, image = cardImage5).pack(side=LEFT)
#c6 = Checkbutton(frame2, image = cardImage6).pack(side=LEFT)
#c7 = Checkbutton(frame2, image = cardImage7).pack(side=LEFT)



window.mainloop()

submitted = []

for i in range(len(varList)):
    if varList[i].get() == 1:
        submitted.append(Stephen.hand[i])

for i in range(len(submitted)):
    print(submitted[i])


for i in range(len(submitted)):
    if submitted[i].get_card_suit() != "Hearts":
        print(str(submitted[i]) + " is not a heart")

    if submitted[i].get_card_suit() == "Hearts":
        print(str(submitted[i]) + " is a Heart!")   # Points will be awarded here





