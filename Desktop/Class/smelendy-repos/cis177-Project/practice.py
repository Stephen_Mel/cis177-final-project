from tkinter import *  # import tkinter
import random

'''
SIZE_OF_HAND = 5

class DeckOfCardsGUI:
    def __init__(self):
        window = Tk()
        window.title("Pick A Hand of Cards Randomly")

        self.imageList = []
        for i in range(1, 53):
            self.imageList.append(PhotoImage(file="cards/" + str(i) + ".gif"))

        frame = Frame(window)
        frame.pack()

        self.labelList = []
        for i in range(SIZE_OF_HAND):
            self.labelList.append(Label(frame, image=self.imageList[i]))
            self.labelList[i].pack(side=LEFT)

        Button(window, text="Shuffle", command=self.shuffle).pack()

        window.mainloop()


    def shuffle(self):
        random.shuffle(self.imageList)
        for i in range(SIZE_OF_HAND):
            self.labelList[i]["image"] = self.imageList[i]

DeckOfCardsGUI()
'''

window = Tk()
window.title("Practice")

frame = Frame(window)
frame.pack()

cardImage1 = PhotoImage(file="cards/" + str(1) + ".gif")

cardImage2 = PhotoImage(file="cards/" + str(2) + ".gif")

Label(frame, image=PhotoImage(file="cards/" + str(1) + ".gif")).pack(side=LEFT)
Label(frame, image=cardImage1).pack(side=LEFT)
Label(frame, image=cardImage2).pack(side=LEFT)

window.mainloop()

