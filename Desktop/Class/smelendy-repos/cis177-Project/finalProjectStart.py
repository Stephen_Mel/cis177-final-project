import random
from tkinter import *



class StandardDeck:

    # The following are static variables of the StandardDeck class
    suits = ["Spades", "Diamonds", "Clubs", "Hearts"]
    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King"]
    size = len(suits) * len(ranks)


#-----------------------------------------------------------------------
class Card:
    # Initializer (constructor)
    def __init__(self, value):

        self.__value = value
        self.__suit = StandardDeck.suits[self.__value // 13]
        self.__rank = StandardDeck.ranks[self.__value % 13]
        self.imagePath = "cards/" + str(self.__value+1) + ".gif"

    # Return the string representation of this card
    def __str__(self):

        string = self.__rank + " of " + self.__suit

        return string
    # Return the integer value of this card (Accessor method for __value)
    def get_card_int(self):
        return self.__value


#-----------------------------------------------------------------------    

#deck = list(range(0, StandardDeck.size))
#shuffle(deck)

#print(deck)

deck = []


for i in range(0, StandardDeck.size):
    card = Card(i)      # constructs a Card object ... calls the initializer
    #print(card, "(" + str(Card.get_card_int(card)) + ")")
    deck.append(card)

#print(deck)

class Player:
    def __init__(self, handSize=7):
        self.handSize = handSize
        self.hand = []

    def drawHand(self):
        count = 0
        while len(self.hand) < self.handSize:
            self.hand.append(deck[0])
            deck.remove(deck[0])
            count += 1

class Fizbin:
    def __init__(self):

        moods = ["Agreeable", "Neutral", "Miserable"]
        self.__agreeability = random.randint(1,3)
        self.__mood = moods[self.__agreeability]


random.shuffle(deck)

Stephen = Player()

Stephen.drawHand()



for i in range(0, len(Stephen.hand)):
    print(str(Stephen.hand[i]))

print()

for i in range(0, len(deck)):
    print(str(deck[i]))

'''
class DeckOfCardsGUI:
    def __init__(self):
        window = Tk()
        window.title("Pick A Hand of Cards Randomly")

        self.imageList = []
        for i in range(1, 53):
            self.imageList.append(PhotoImage(file="cards/" + str(i) + ".gif"))

        frame = Frame(window)
        frame.pack()

        self.labelList = []
        for i in range(SIZE_OF_HAND):
            self.labelList.append(Label(frame, image=self.imageList[i]))
            self.labelList[i].pack(side=LEFT)

        Button(window, text="Shuffle", command=self.shuffle).pack()

        window.mainloop()


    def shuffle(self):
        random.shuffle(self.imageList)
        for i in range(SIZE_OF_HAND):
            self.labelList[i]["image"] = self.imageList[i]

'''

window = Tk()
window.title("Draw a hand")

frame = Frame(window)
frame.pack()


#cardImage = PhotoImage(file=Stephen.hand[0].imagePath)

#Label(frame, image=cardImage).pack(side=LEFT)
'''
for i in range(0, len(Stephen.hand)):
    Label(frame, image=PhotoImage(file=Stephen.hand[i].imagePath)).pack(side=LEFT)
'''

labelList = []
for i in range(0, len(Stephen.hand)):

    cardValue = Stephen.hand[i].get_card_int()
    cardImage = PhotoImage(file="cards/" + str(cardValue+1) + ".gif")
    label = Label(frame, image=cardImage)
    labelList.append(label)


for i in range(0, len(labelList)):
    labelList[i].pack(side=LEFT)


'''

cardValue1 = Stephen.hand[0].get_card_int()
cardImage1 = PhotoImage(file="cards/" + str(cardValue1+1) + ".gif")
Label(frame, image=cardImage1).pack(side=LEFT)

cardValue1 = Stephen.hand[1].get_card_int()
cardImage1 = PhotoImage(file="cards/" + str(cardValue1+1) + ".gif")
Label(frame, image=cardImage1).pack(side=LEFT)
'''
'''
self.labelList = []
for i in range(SIZE_OF_HAND):
    self.labelList.append(Label(frame, image=self.imageList[i]))
    self.labelList[i].pack(side=LEFT)
'''

window.mainloop()

#cardImage2 = PhotoImage(file="cards/" + str(2) + ".gif")