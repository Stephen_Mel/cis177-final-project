Name: Stephen Melendy

Individual Final Project (Fizbin)


Program Vision:

Fizbin is a game of wacky rules. To create context for the unpredictable nature of the game I created an evil Genie named Fizbin. The object of the game is give Fizbin the cards he requests. This can be complicated due to his moody nature and strange requests.  


Gameplay:

Each player will be given a hand of cards. The game will run over a series of rounds. Each round begins with Fizbin requesting cards ("Give me your pairs", "Submit any Hearts in your hand", etc.). Depending on his current mood your cards will either be accepted (and points will be awarded) or they will be rejected. Maximum points will go to those who test their luck. 


Current State:

To be honest, I struggled a lot with Tkinter. After dozens of failed for-loops, I eventually just ended up typing everything out (instead of making window class instances). The code is kinda a nightmare, but from a user's standpoint it functions how I wanted it to function.

Please install the "Chiller" font included in the repository.


Issues:

I know that the code could be a LOT more concise, a lot of stuff is repeated. I would have also liked to have had a round that asked for a run of cards, but I struggled to figure out an easy way to implement this.